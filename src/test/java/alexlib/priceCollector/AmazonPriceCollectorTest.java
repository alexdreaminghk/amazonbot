package alexlib.priceCollector;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import alexlib.entity.Item;


public class AmazonPriceCollectorTest  {

    @Mock
    public HttpClient httpClient;

    private final String userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36";
    private AmazonPriceCollector priceCollector;

    @Mock
    public Item item;

    @BeforeAll
    public void init() {
        httpClient = Mockito.mock(HttpClient.class);
        //Assertions.assertNotNull(httpClient);
        priceCollector = new AmazonPriceCollector(httpClient, userAgent);

        HttpResponse respone = Mockito.mock(HttpResponse.class);

        Mockito.when(respone.body()).thenReturn("""
        <!DOCTYPE html>
        <html>
           <head>
              <title>Page Title</title>
           </head>
           <body>
               <span id="priceblock_ourprice" class="a-size-medium a-color-price priceBlockBuyingPriceString">£21.06</span>
           </body>
        </html>
        
        """);

        try {
            Mockito.when(httpClient.send(Mockito.any(), Mockito.any())).thenReturn(respone);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        
    }

    @Test
    public void getPrice_test1(){

        Item item = new Item("https://fakeUrl");
        priceCollector.getPrice(item);
        Assertions.assertTrue(item.getPrices().size()>0);
    }

    @Test
    public void getPrice_test2(){

        //Item item = new Item("https://fakeUrl");
        //AmazonPriceCollector.getPrice(item);
        //Assertions.assertTrue(item.getPrices().size()>0);

        //TODO to be implement
        Assertions.assertTrue(false);
    }

    @Test
    public void getPriceValue_Test1() {
        Assertions.assertTrue(AmazonPriceCollector.getPriceValue("£140.44").compareTo(new BigDecimal("140.44")) == 0);
    }

    @Test
    public void getPriceValue_Test2() {
        Assertions.assertTrue(AmazonPriceCollector.getPriceValue("$140.44").compareTo(new BigDecimal("140.44")) == 0);
    }

    @Test
    public void getProductDesc_Test1() {
        String desc = AmazonPriceCollector.getProductDesc(
                "Kindle Paperwhite | Waterproof, 6&quot; High-Resolution Display, 8 GB—with Ads—Twilight Blue : Amazon.co.uk: Electronics &amp; Photo");
        Assertions.assertEquals(
                "Kindle Paperwhite | Waterproof, 6&quot; High-Resolution Display, 8 GB—with Ads—Twilight Blue ", desc);
    }

}
