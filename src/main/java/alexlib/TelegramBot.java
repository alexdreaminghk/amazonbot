package alexlib;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.URLEncoder;
import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TelegramBot {

    private static Logger logger = LoggerFactory.getLogger(TelegramBot.class);

    private static final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2)
            .connectTimeout(Duration.ofSeconds(10)).build();
    private static final String userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36";

    private static final String botToken = "1920802810:AAG-CQ9Hy1XOXQadovuSh-dcQmKjwl3ilyU";
    private static final String chat_id = "1166569951";


    //Reference https://rieckpil.de/howto-send-telegram-bot-notifications-with-java/
    public static void main(String[] args) {
        // to get the chat_id
        // https://api.telegram.org/bot1920802810:AAG-CQ9Hy1XOXQadovuSh-dcQmKjwl3ilyU/getUpdates
        sendMessage("Hialex!");
        
    }

    public static void sendMessage(String text){
        String content;
        try {
            content = URLEncoder.encode(text, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            logger.error("Message encoding fails.", e1);
            content = "[Message encode error]";
        }
        String url = "https://api.telegram.org/bot"+botToken+"/sendMessage?chat_id="+chat_id+"&text="+content;

        

        HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create(url))
                .setHeader("User-Agent", userAgent) // add request header
                .GET()
                .build();

        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            System.out.println(response.body());
        } catch (IOException e) {
            logger.error("", e);
        } catch (InterruptedException e) {
            logger.error("", e);
        }
    }
}
