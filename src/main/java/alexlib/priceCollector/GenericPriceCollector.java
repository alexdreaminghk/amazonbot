package alexlib.priceCollector;

import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import alexlib.entity.Item;
import alexlib.entity.Price;
import alexlib.priceCollector.siteStrategy.WebsiteStrategy;

public class GenericPriceCollector {

    private HttpClient httpClient;
    private String userAgent;
    private WebsiteStrategy websiteStrategy;
    private static final Logger logger = LoggerFactory.getLogger(GenericPriceCollector.class);

    public GenericPriceCollector(HttpClient httpClient, String userAgent, WebsiteStrategy websiteStrategy) {
        this.httpClient = httpClient;
        this.userAgent = userAgent;
        this.websiteStrategy = websiteStrategy;
    }

    public void getPrice(Item item) {
        try {
            HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create(item.getUrl()))
                    .setHeader("User-Agent", userAgent) // add request header
                    .build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            Document document = Jsoup.parse(response.body(), "utf-8");

            BigDecimal priceVal;

            if (!websiteStrategy.isOutOfStock(document)) {
                // in stock
                String priceStr = websiteStrategy.getItemPriceStr(document);
                logger.info("Price: " + getPriceValue(priceStr));
                priceVal = getPriceValue(priceStr);
            } else {
                // out of stock
                priceVal = BigDecimal.valueOf(-1);
            }

            Price price = new Price(new Date(), priceVal);
            item.addPrice(price);
            item.setItemDesc(websiteStrategy.getItemDesc(document));

        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public static BigDecimal getPriceValue(String input) {
        String output = "";

        Pattern pattern = Pattern.compile("\\d{1,3}[,\\.]?(\\d{1,2})?");
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            output = matcher.group(0);
            output = output.replaceAll(",", "");
        }
        return new BigDecimal(output);
    }

    public HttpClient getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }
}
