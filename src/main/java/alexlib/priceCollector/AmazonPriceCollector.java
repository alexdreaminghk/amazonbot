package alexlib.priceCollector;

import java.math.BigDecimal;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.net.http.HttpClient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import alexlib.entity.Item;
import alexlib.entity.Price;

public class AmazonPriceCollector {

    private HttpClient httpClient;
    private String userAgent;

    public AmazonPriceCollector(HttpClient httpClient, String userAgent){
        this.httpClient = httpClient;
        this.userAgent = userAgent;
    }

    public void getPrice(Item item) {
        try {
            HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create(item.getUrl()))
                    .setHeader("User-Agent", userAgent) // add request header
                    .build();

            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

            Document document = Jsoup.parse(response.body(), "utf-8");

            //check if it is out of stock
            Element outOfStock = document.selectFirst("#outOfStock");

            Element priceElement;
            BigDecimal priceVal;

            if (outOfStock==null)
            {
                //in stock
                priceElement = document.selectFirst("#priceblock_ourprice");

                System.out.println("Price: " + getPriceValue(priceElement.text()));

                priceVal = getPriceValue(priceElement.text());
            }
            else
            {
                //out of stock
                priceVal = BigDecimal.valueOf(-1);
            }

            Price price = new Price(new Date(), priceVal);
            item.addPrice(price);
            item.setItemDesc(getProductDesc(document.title()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static BigDecimal getPriceValue(String input) {
        String output = "";

        Pattern pattern = Pattern.compile("\\d{1,3}[,\\.]?(\\d{1,2})?");
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            output = matcher.group(0);
            output = output.replaceAll(",", "");
        }
        return new BigDecimal(output);
    }

    public static String getProductDesc(String input){
        return Arrays.asList(input.split(":")).get(0);
    }


    public HttpClient getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }
}
