package alexlib.priceCollector.siteStrategy;

import java.util.Arrays;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class AmazonStrategy implements WebsiteStrategy {

    @Override
    public boolean isOutOfStock(Document document) {
        Element outOfStock = document.selectFirst("#outOfStock");
        return outOfStock==null;
    }

    @Override
    public String getItemDesc(Document document) {
        return getProductDesc(document.title());
    }

    @Override
    public String getItemPriceStr(Document document) {
        Element priceElement = document.selectFirst("#priceblock_ourprice");
        return priceElement.text();
    }

    private static String getProductDesc(String input) {
        return Arrays.asList(input.split(":")).get(0);
    }
    
}
