package alexlib.priceCollector.siteStrategy;

import org.jsoup.nodes.Document;

public interface WebsiteStrategy {
    public boolean isOutOfStock(Document document);

    public String getItemDesc(Document document);

    public String getItemPriceStr(Document document);
}
