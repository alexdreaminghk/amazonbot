package alexlib.priceCollector.siteStrategy;

import java.util.Arrays;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ArgosStrategy implements WebsiteStrategy {

    Logger logger = LoggerFactory.getLogger(ArgosStrategy.class);

    @Override
    public boolean isOutOfStock(Document document) {
        Element outOfStock = document.getElementById("h1title");
        System.out.println(document.text());
        if (outOfStock.text().contains("is currently unavailable."))
            return true;
        else
            return false;
    }

    @Override
    public String getItemDesc(Document document) {
        try
        {
            Element element = document.selectFirst("[itemprop=name]");
            return element.text();
            
        }
        catch (Exception e)
        {
            logger.error("Get item desc failed",e);
            return "Error description";
        }
    }

    @Override
    public String getItemPriceStr(Document document) {
        try
        {
            Element priceElement = document.selectFirst("[itemprop=price]");
            return priceElement.attributes().get("content");
        }
        catch (Exception e)
        {
            logger.error("Get item price failed",e);
            return "0";
        }
        
    }
}
