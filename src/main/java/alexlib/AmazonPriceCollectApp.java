// package alexlib;

// import java.net.http.HttpClient;
// import java.time.Duration;
// import java.util.List;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.context.ApplicationContext;
// import org.springframework.context.annotation.AnnotationConfigApplicationContext;
// import org.springframework.stereotype.Component;

// import alexlib.config.ApplicationConfiguration;
// import alexlib.entity.Item;
// import alexlib.config.MongoConfig;
// import alexlib.priceCollector.AmazonPriceCollector;
// import alexlib.repository.ItemRepository;

// @Component
// public class AmazonPriceCollectApp {

//     private HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_1_1)
//     .connectTimeout(Duration.ofSeconds(10)).build();

//     @Autowired
//     private ItemRepository itemRepo;
    
//     @SuppressWarnings("checked")
//     public static void main(String[] args) {

//         ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfiguration.class, MongoConfig.class);
//         ctx.getBean(AmazonPriceCollectApp.class).run();
//     }

//     public void run(){
//         Item item1 = new Item(
//             "https://www.amazon.co.uk/gp/product/B07VWL8671/ref=ox_sc_act_title_1?smid=A3P5ROKL5A1OLE&psc=1");

//         AmazonPriceCollector.getPrice(item1); //"Gore-tex Jacket"

//         System.out.println(item1);

//         List<Item> items = itemRepo.findByUrl(item1.getUrl());

//         if (items.size()>0)
//         {
//             Item existingItem = items.get(0);
//             existingItem.addPrice(item1.getPrices().get(0));
//             itemRepo.save(existingItem);
//             System.out.println("Price has been added to database.");
//         }
//         else
//         {
//             itemRepo.save(item1);
//             System.out.println("Item has been created in database.");
//         }

        

//     }
// }
