package alexlib;

import java.net.http.HttpClient;
import java.time.Duration;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import alexlib.config.*;
import alexlib.entity.*;
import alexlib.repository.*;
import alexlib.priceCollector.AmazonPriceCollector;
import alexlib.priceCollector.GenericPriceCollector;
import alexlib.priceCollector.siteStrategy.AmazonStrategy;
import alexlib.priceCollector.siteStrategy.ArgosStrategy;
import alexlib.priceCollector.siteStrategy.WebsiteStrategy;

@Component
public class App {


    private static final Logger logger = LoggerFactory.getLogger(App.class);


    private HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_1_1).connectTimeout(Duration.ofSeconds(10)).build();
    private static final String userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15";


    @Autowired
    private ItemRepository itemRepo;
    
    @SuppressWarnings("resource")
    public static void main(String[] args) {

        ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfiguration.class, MongoConfig.class);
        ctx.getBean(App.class).run();
    }

    public void run(){

        List<Item> itemList = itemRepo.findAll();

        for (Item item: itemList)
        {
            logger.info("=====Item "+(itemList.indexOf(item)+1)+" has been processing=====");
            getAndUpdateItem(item);
            logger.info("=====Item "+(itemList.indexOf(item)+1)+" has been processed=====");
        }
    }

    public void getAndUpdateItem(Item item1){

        WebsiteStrategy websiteStrategy;
        if (item1.getUrl().contains("www.amazon.co.uk"))
        {
            websiteStrategy = new AmazonStrategy();
        }
        else if (item1.getUrl().contains("www.argos.co.uk"))
        {
            websiteStrategy = new ArgosStrategy();
        }
        else
        {
            throw new RuntimeException("No correct website strategy. url=["+item1.getUrl()+"]");
        }
        GenericPriceCollector priceCollector = new GenericPriceCollector(httpClient, userAgent, websiteStrategy);
        //get latest price and add it to the item.prices
        priceCollector.getPrice(item1);

        logger.info("Item details: "+item1.toString());
        itemRepo.save(item1);
        logger.info("Item and Price has been updated to database.");

        Integer priceListSize = item1.getPrices().size();
        String priceValueStr = item1.getPrices().get(priceListSize-1).getPrice().intValue()==-1? "Out of stock" : item1.getPrices().get(priceListSize-1).getPrice().toString();

        //send telegram message when price updated. Must have at least 2 price value
        logger.info("item1.getPrices().size()=["+item1.getPrices().size()+"]");
        if (item1.getPrices().size()>1)
        {
            Price lastPrice = item1.getPrices().get(item1.getPrices().size()-2);
            Price currentPrice = item1.getPrices().get(item1.getPrices().size()-1);

            if (lastPrice.getPrice().equals(currentPrice.getPrice()))
            {
                // TelegramBot.sendMessage(
                //     "Item: "+item1.getItemDesc()+"\n"
                //     +"Price has no change and the current price is: "+priceValueStr+" \n"
                //     +"Last Update: "+lastPrice);
                logger.info("No Price change do not send message");
            }
            else
            {
                TelegramBot.sendMessage(
                    "Item: "+item1.getItemDesc()+"\n"
                    +"Price change from "+lastPrice.getPrice()+" to "+priceValueStr+" \n"
                    +"Last Update: "+lastPrice);
                    logger.info("Telegram message send. Price changed.");
            }
        }
    }
}
