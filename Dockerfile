FROM dreaminghk/jdk17-base:latest AS build
#VOLUME /tmp
ARG JAVA_OPTS
ENV JAVA_OPTS=$JAVA_OPTS
COPY pom.xml /
RUN mvn -B dependency:resolve-plugins dependency:resolve
COPY . /
RUN mvn clean package
#COPY target/amzbotvs-1.0-SNAPSHOT.jar amzbotvs.jar


FROM openjdk:17-jdk-alpine
RUN apk add --update apk-cron && rm -rf /var/cache/apk/*
COPY --from=build target/amzbotvs-1.0-SNAPSHOT-jar-with-dependencies.jar amzbotvs.jar
ADD crontab.txt /crontab.txt
ADD script.sh /script.sh
COPY entry.sh /entry.sh
RUN chmod 755 /script.sh /entry.sh
RUN /usr/bin/crontab /crontab.txt
CMD /entry.sh
#ENTRYPOINT exec java $JAVA_OPTS -jar amzbotvs.jar
